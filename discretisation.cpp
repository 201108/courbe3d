
#include "discretisation.h"

QVector<float>* discretize(Point &p, float r, float v, float b){
    QVector<float> *discretisation = new QVector<float>;

    discretisation->push_back(p.getX());
    discretisation->push_back(p.getY());
    discretisation->push_back(p.getZ());

    discretisation->push_back(r);
    discretisation->push_back(v);
    discretisation->push_back(b);

    return discretisation;
}

QVector<float>* discretizeQUADS(SurfaceParametrique &p, float stepU, float stepV, float begin, float end){
    QVector<float> *discretisation = new QVector<float>;

    for(float i=begin; i<end-stepV/2.; i+=stepV){
        float r=(std::rand()%255)/255. ,v=(std::rand()%255)/255. ,b=(std::rand()%255)/255.;
        qDebug()<<"I"<<i;
        for(float j=begin; j<end-stepU/2.; j+=stepU){
            qDebug()<<"J"<<j;
            //qDebug() << "ZT" << i/(1.*(end-step/2.)) << j/(1.*(end-step/2.));
            Point P00 = p.getPoint(i,j);
            Point P01 = p.getPoint(i,j+stepU);
            Point P10 = p.getPoint(i+stepU,j);
            Point P11 = p.getPoint(i+stepU,j+stepU);
            //qDebug() << "AT";

            discretisation->append(*discretize(P00, r, v, b));
            //qDebug() << "RT";
            discretisation->append(*discretize(P01, r, v, b));
            //qDebug() << "RT";
            discretisation->append(*discretize(P11, r, v, b));
            //qDebug() << "RT";
            discretisation->append(*discretize(P10, r, v, b));
            //qDebug() << "RT";
        }
    }
    qDebug() << "Sortie";
    return discretisation;
}

QVector<float>* discretize(CourbeParametrique &p, /*std::function<float(float)> lambda, */float step, float begin, float end){
    QVector<float> *discretisation = new QVector<float>;
    if(end < begin){qDebug() << "DEF" ;return discretisation;}

    Point actual;
    float r=(std::rand()%255)/255. ,v=(std::rand()%255)/255. ,b=(std::rand()%255)/255.;
    for(float x=begin; x < end; x+=step){
        //Point actual = p.getPoint(lambda(x));
        actual = p.getPoint(x);

        //Coordonnées sommet
        discretisation->push_back(actual.getX());
        discretisation->push_back(actual.getY());
        discretisation->push_back(actual.getZ());

        //Couleur du sommet
        discretisation->push_back(r);
        discretisation->push_back(v);
        discretisation->push_back(b);

    }

    qDebug() << "NbVertex:> " << discretisation->size()/6;
    return discretisation;
}

QVector<float>* discretize(SurfaceParametrique &p, /*std::function<float(float)> lambda, */float stepU, float stepV, float begin, float end){
    /*QVector<float> *discretisation = new QVector<float>;

    Point actual;
    float r=(std::rand()%255)/255. ,v=(std::rand()%255)/255. ,b=(std::rand()%255)/255.;
    for(float u=begin; u <= end; u+=stepU)
        for(float v=begin; v <= end; v+=stepV){
            qDebug() << "disc";
            //Point actual = p.getPoint(lambda(x));
            actual = p.getPoint(u, v);
            qDebug() << "(u,v)" << u<< " " << v <<actual.getX() << actual.getY() << actual.getZ();
            //Coordonnées sommet
            discretisation->push_back(actual.getX());
            discretisation->push_back(actual.getY());
            discretisation->push_back(actual.getZ());

            //Couleur du sommet
            discretisation->push_back(r);
            discretisation->push_back(v);
            discretisation->push_back(b);

        }
    return discretisation;*/
    return discretizeQUADS(p, stepU, stepV, begin, end);
}

template<typename T> bool helper(T val) {
    std::numeric_limits<T> limit;
    return limit.min() <= val && val <= limit.max();
}

QVector<float>* findPoint(SurfaceParametrique &p, float u, float v){
    QVector<float> *discretisation = new QVector<float>;
    float r= 1.0f , g= 0.1f, b= 0.1f;
    Point actual = p.getPoint(u, v);
    //Si une de nos valeur est pas bonne !
    /*if(!helper<float>(actual.getX()) || !helper<float>(actual.getY()) || !helper<float>(actual.getZ())) {
            return findPoint(p, u, v);
    }*/
    //Coordonnées sommet
    discretisation->push_back(actual.getX());
    discretisation->push_back(actual.getY());
    discretisation->push_back(actual.getZ());

    qDebug() << "MITSUKETA :" << actual.getX() << actual.getY() << actual.getZ();
    //Couleur du sommet
    discretisation->push_back(r);
    discretisation->push_back(g);
    discretisation->push_back(b);

    return discretisation;
}
