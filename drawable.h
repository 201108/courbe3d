#ifndef DRAWABLE_H
#define DRAWABLE_H

class Drawable{
    virtual int getDrawMethod()=0;
    int getIndex();
    int setIndex(int index);
protected:
    int index;
};

#endif // DRAWABLE_H
