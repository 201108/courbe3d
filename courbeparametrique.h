#ifndef COURBEPARAMETRIQUE_H
#define COURBEPARAMETRIQUE_H
#include "point.h"

class CourbeParametrique
{
public:
    CourbeParametrique(){}
    //~CourbeParametrique(){};
    virtual Point getPoint(float x)=0;
};

#endif // COURBEPARAMETRIQUE_H
