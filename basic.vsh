attribute highp vec4 posAttr;
//attribute vec3 normal;

attribute lowp vec4 colAttr;
varying lowp vec4 col;

varying vec3 vert;
//varying vec3 vertNormal;

uniform mat4 projMatrix;
uniform mat4 mvMatrix;
uniform highp mat3 normalMatrix;

void main() {
   //vert = posAttr.xyz;
   col = colAttr;
   //vertNormal = normalMatrix * normal;
   gl_Position = projMatrix * mvMatrix * posAttr;
}
