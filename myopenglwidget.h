#ifndef MYOPENGLWIDGET_H
#define MYOPENGLWIDGET_H

#include <QObject>
#include <QWidget>

#include <QKeyEvent>
#include <QTimer>
#include <QOpenGLWidget>
#include <QOpenGLFunctions>
#include <QOpenGLBuffer>
#include <QOpenGLShaderProgram>

#include "point.h"

class myOpenGLWidget : public QOpenGLWidget,
			   protected QOpenGLFunctions
{
	Q_OBJECT

public:
	explicit myOpenGLWidget(QWidget *parent = nullptr);
	~myOpenGLWidget();

signals:  // On ne les implémente pas, elles seront générées par MOC ;
		  // les paramètres seront passés aux slots connectés.

public slots:
    void setPoint(float x, float y);
    void setStepU(int step);
    void setStepV(int step);

protected slots:
	void onTimeout();

protected:
	void initializeGL() override;
	void doProjection();
	void resizeGL(int w, int h) override;
	void paintGL() override;
	void keyPressEvent(QKeyEvent *ev) override;
	void keyReleaseEvent(QKeyEvent *ev) override;
	void mousePressEvent(QMouseEvent *ev) override;
	void mouseReleaseEvent(QMouseEvent *ev) override;
	void mouseMoveEvent(QMouseEvent *ev) override;
    void setZRotation(int angle);
    void setYRotation(int angle);
    void setXRotation(int angle);
    QVector<float> *initVector();
    QVector<float> *vecOfObj();

public :
    bool protect = false;
    QVector2D * compute = new QVector2D(0.0f, 0.0f);
    double stepU = 0.1;
    double stepV = 0.1;
    QVector<QVector3D> mesh;

private:

    QPoint m_last_pos;
    double m_angle_X = 0;
    double m_angle_Y = 0;
    double m_angle_Z = 0;
	double m_angle = 0;
	QTimer *m_timer = nullptr;
	double m_ratio = 1;


	//RR matrices utiles
    //RR matrices utiles
    QMatrix4x4 m_proj;
    QMatrix4x4 m_world;
    QMatrix4x4 m_camera;
    int m_projMatrixLoc;
    int m_mvMatrixLoc;
    int m_lightPosLoc;
    int m_normalMatrixLoc;

	QOpenGLShaderProgram *m_program;
    QOpenGLBuffer m_vbo;
    QVector2D helper(int size);
    QVector<float> *patch(QVector<QVector<Point>> car);
public :
	void makeGLObjects();
	void tearGLObjects();
    void majGLObject();

signals:
    void xRotationChanged(double angle);
    void yRotationChanged(double angle);
    void zRotationChanged(double angle);
};


#endif // MYOPENGLWIDGET_H
