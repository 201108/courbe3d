/* R. Raffin
 * M1 Informatique, Aix-Marseille Université
 * Fenêtre principale
 * Au cas où, l'UI contient une barre de menu, une barre de status, une barre d'outils (cf QMainWindow).
 * Une zone est laissée libre à droite du Widget OpenGL pour mettre de futurs contrôles ou informations.
 */

#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
	QMainWindow(parent),
	ui(new Ui::MainWindow)
{

	ui->setupUi(this);
    connect(ui->loadOBJ, SIGNAL(clicked()), this, SLOT(onLoadButtonPressed()));
    connect(ui->ddSlider, SIGNAL(mousePressed()), this, SLOT(setPoint()));
    //connect(ui->uVal, SIGNAL(valueChanged(int)), this, SLOT(setStepU()));
    //connect(ui->vVal, SIGNAL(valueChanged(int)), this, SLOT(setStepV()));
    //connect(ui->ddSlider, SIGNAL(frameSwapped()), this, SLOT(setPoint()));
    connect(ui->ddSlider, &DDSlider::posChanged, ui->openGLWidget, &myOpenGLWidget::setPoint);
    connect(ui->uVal, &QSlider::valueChanged, ui->openGLWidget, &myOpenGLWidget::setStepU);
    connect(ui->vVal, &QSlider::valueChanged, ui->openGLWidget, &myOpenGLWidget::setStepV);
    //timer
}

MainWindow::~MainWindow()
{
	delete ui;
}

void MainWindow::onLoadButtonPressed() {

    // fenêtre de sélection des fichiers
    QString fileName = QFileDialog::getOpenFileName(this, tr("Open Mesh"), "", tr("Mesh Files (*.obj)"));

    QFile objFile(fileName);
    if(!objFile.exists()){
        qDebug() << "File not found";
        return;
    }

    MeshLoader m(fileName);

    MyMesh * _mesh = new MyMesh();
    ui->openGLWidget->mesh = m.parse_mesh_file(*_mesh);
}

bool MainWindow::isValid(QVector2D * pt) {
    if(pt == nullptr || pt  == NULL || pt == 0 || !pt) {
        qDebug() << (pt == nullptr) << (pt  == NULL) << (pt == 0) << !pt;
        return false;
    }
    return (0.0f <= pt->x() && pt->x() <= 1.0f) && (0.0f <= pt->y() && pt->y() <= 1.0f);
}

void MainWindow::setPoint() {
    if(!isValid(ui->ddSlider->current)) return;
    ui->openGLWidget->protect = true;
    ui->openGLWidget->compute = ui->ddSlider->current;
    //ui->openGLWidget->makeGLObjects();
}

void MainWindow::setStepU() {
    ui->openGLWidget->stepU = (float)1 / (float)ui->uVal->value();
    ui->openGLWidget->makeGLObjects();
}

void MainWindow::setStepV() {
    ui->openGLWidget->stepV = (float)1 / (float)ui->vVal->value();
    ui->openGLWidget->makeGLObjects();
}
