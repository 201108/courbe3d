varying highp vec4 col;
void main() {
    gl_FragColor = col;
}

//varying highp vec3 nor;
/*
void main() {
    // Direction de lumière normalisée
    vec3 light = normalize(vec3(0.0, 0.0, 10.0));

        // Normale du fragment, normalisée
    vec3 nor3 = normalize(vec3(1.0, 1.0, 1.0));

        // Cosinus de l'angle entre la normale et la lumière
        // borné entre 0 et 1
        //float cosTheta = clamp( dot(nor3,light), 0.0 , 1.0 );
    float cosTheta = clamp( dot(nor3,light ), 0.3 , 1.0 );

    // Couleur de la lumière
    vec3 lightColor = col.xyz;

    //gl_FragColor = vec4(nor3, 1.0);
    gl_FragColor = vec4(lightColor * cosTheta, 1.0);
    //gl_FragColor = texture2D(texture, texc.st);
}*/

