#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QFileDialog>
#include <QVector2D>

#include "meshloader.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
	Q_OBJECT

public:
	explicit MainWindow(QWidget *parent = 0);
	~MainWindow();

    bool isValid(QVector2D *pt);

protected slots:
    void onLoadButtonPressed();
    void setPoint();
    void setStepU();
    void setStepV();
private:
	Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
