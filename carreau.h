#ifndef CARREAU_H
#define CARREAU_H
#include "surfaceparametrique.h"

class Carreau : public SurfaceParametrique
{
public:
    Carreau(QVector<QVector<Point>> array);
    Point getPoint(float x, float y)override;
private:
    QVector<QVector<Point>> array;
};

#endif // CARREAU_H
