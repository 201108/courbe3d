#ifndef CHAIKIN_H
#define CHAIKIN_H
#include "courbeparametrique.h"
#include "segment.h"
#include <QVector>

class Chaikin : public CourbeParametrique
{
public:
    Chaikin(QVector<Point> points);
    Point getPoint(float x)override;
    QVector<Segment> iterations(QVector<Segment> segments, int level);
private:
    QVector<Point> points;
};

#endif // CHAIKIN_H
