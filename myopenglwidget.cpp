//R. Raffin, M1 Informatique, "Surfaces 3D"
//tiré de CC-BY Edouard.Thiel@univ-amu.fr - 22/01/2019

#include "myopenglwidget.h"
#include <QDebug>
#include <QSurfaceFormat>
#include <QMatrix4x4>

#include <iostream>

#include "segment.h"
#include "discretisation.h"
#include "bezier.h"
#include "carreau.h"

static const QString vertexShaderFile   = ":/basic.vsh";
static const QString fragmentShaderFile = ":/basic.fsh";
int nb_vertex=0;

myOpenGLWidget::myOpenGLWidget(QWidget *parent) :
	QOpenGLWidget(parent)
{
	qDebug() << "init myOpenGLWidget" ;

	QSurfaceFormat sf;
	sf.setDepthBufferSize(24);
	sf.setSamples(16);  // nb de sample par pixels : suréchantillonnage por l'antialiasing, en décalant à chaque fois le sommet
						// cf https://www.khronos.org/opengl/wiki/Multisampling et https://stackoverflow.com/a/14474260
	setFormat(sf);

	setEnabled(true);  // événements clavier et souris
	setFocusPolicy(Qt::StrongFocus); // accepte focus
	setFocus();                      // donne le focus

	m_timer = new QTimer(this);
	m_timer->setInterval(50);  // msec
	connect (m_timer, SIGNAL(timeout()), this, SLOT(onTimeout()));
}

myOpenGLWidget::~myOpenGLWidget()
{
	qDebug() << "destroy GLArea";

	delete m_timer;

	// Contrairement aux méthodes virtuelles initializeGL, resizeGL et repaintGL,
	// dans le destructeur le contexte GL n'est pas automatiquement rendu courant.
	makeCurrent();
	tearGLObjects();
	doneCurrent();
}


void myOpenGLWidget::initializeGL()
{
	qDebug() << __FUNCTION__ ;
	initializeOpenGLFunctions();
	glEnable(GL_DEPTH_TEST);

	makeGLObjects();

	//shaders
	m_program = new QOpenGLShaderProgram(this);
	m_program->addShaderFromSourceFile(QOpenGLShader::Vertex, vertexShaderFile);  // compile
	m_program->addShaderFromSourceFile(QOpenGLShader::Fragment, fragmentShaderFile);

    m_program->bindAttributeLocation("posAttr", 0);
    m_program->bindAttributeLocation("colAttr", 1);

    if (! m_program->link()) {  // édition de lien des shaders dans le shader program
        qWarning("Failed to compile and link shader program:");
        qWarning() << m_program->log();
    }

    m_program->bind(); // active le shader program
    m_projMatrixLoc = m_program->uniformLocation("projMatrix");
    m_mvMatrixLoc = m_program->uniformLocation("mvMatrix");
    m_normalMatrixLoc = m_program->uniformLocation("normalMatrix");
    m_lightPosLoc = m_program->uniformLocation("lightPos");

    m_camera.setToIdentity();
    m_camera.translate(0, 0, -1.5);

    m_program->setUniformValue(m_lightPosLoc, QVector3D(0, 0, 70));
}

static void qNormalizeAngle(int &angle)
{
    while (angle < 0)
        angle += 360 * 16;
    while (angle > 360 * 16)
        angle -= 360 * 16;
}

void myOpenGLWidget::setXRotation(int angle)
{
    qNormalizeAngle(angle);
    if (angle != m_angle_X) {
        m_angle_X = angle;
        emit xRotationChanged(angle);
        update();
    }
}

void myOpenGLWidget::setYRotation(int angle)
{
    qNormalizeAngle(angle);
    if (angle != m_angle_Y) {
        m_angle_Y = angle;
        emit yRotationChanged(angle);
        update();
    }
}

void myOpenGLWidget::setZRotation(int angle)
{
    qNormalizeAngle(angle);
    if (angle != m_angle_Z) {
        m_angle_Z = angle;
        emit zRotationChanged(angle);
        update();
    }
}

void myOpenGLWidget::doProjection()
{
	//m_mod.setToIdentity();
	//modelMatrix.ortho( -aratio, aratio, -1.0f, 1.0f, -1.0f, 1.0f );
}

//On cherche une partition de notre poly de contrôle
QVector2D myOpenGLWidget::helper(int size) {
    int val = size / 2;
    int mod = 1;
    int a = 0;
    while(mod != 0) {
        a = size / val;
        mod = size % val;
        val --;
    }
    return QVector2D(val, a);
}

QVector<float>* myOpenGLWidget::vecOfObj(){
    if(mesh.isEmpty()) {
        return new QVector<float>();
        //return initVector();
    }
    else {
        QVector2D part = helper(mesh.size());
        QVector<QVector<Point>> car = QVector<QVector<Point>>();
        QVector<float> *tmp = new QVector<float>();
        int cpt = 0;
        for(int i = 0 ; i < part.x(); i++) {
            QVector<Point> points = QVector<Point>();
            for(int j = 0 ; j < part.y(); j++) {
                points.push_back(Point(mesh.at(cpt).x(), mesh.at(cpt).y(), mesh.at(cpt).z()));
                cpt ++;
            }
            car.push_back(points);
        }
        return patch(car);
    }
}

float X=0, Y, Z;
QVector<float>* myOpenGLWidget::initVector(){
    Point A, B, C, D, E, F;
    float * coord = new float[3];

    coord[0] = -1.f+X; coord[1] = 0.f; coord[2] = 0.5f;
    A.set (coord);

    coord[0] = 0.f; coord[1] = 1.f; coord[2] = 0.5f;
    B.set(coord);

    coord[0] = 1.f; coord[1] = 0.f; coord[2] = 0.5f;
    C.set(coord);

    coord[0] = -1; coord[1] = 0; coord[2] = -0.5f;
    D.set(coord);

    coord[0] = 0; coord[1] = 0.5f; coord[2] = -0.5f;
    E.set(coord);

    coord[0] = 1; coord[1] = 0; coord[2] = -0.5f;
    F.set(coord);

    QVector<QVector<Point>> car = QVector<QVector<Point>>();
    QVector<Point> points = QVector<Point>();
    points.push_back(A);
    points.push_back(B);
    //points.push_back(C);
    car.push_back(points);
    points = QVector<Point>();
    points.push_back(D);
    points.push_back(E);
    //points.push_back(F);
    car.push_back(points);
    delete [] coord;
    return patch(car);
}

QVector<float> *myOpenGLWidget::patch(QVector<QVector<Point>> car){
    Carreau c(car);

    QVector<float> *vertData = new QVector<float>();
    /*QVector<float> *unique = new QVector<float>();

    for(QVector<Point> abc: car)
        for(Point p:abc){
            vertData->push_back(p.getX());
            vertData->push_back(p.getY());
            vertData->push_back(p.getZ());

            vertData->push_back(0.2f);
            vertData->push_back(0.8f);
            vertData->push_back(0.1f);
        }
    Bezier op1(car[0]);
    Bezier op2(car[1]);
    vertData->append(*discretize(op1, stepU, 0, 1));
    vertData->append(*discretize(op2, stepU, 0, 1));*/
    vertData->append(*discretize(c, stepU, stepV, 0, 1));
    /*qDebug() << "DEBUG FLAG : " << compute->x() << compute->y();
    if(protect) {
        QVector<float> tmp = *findPoint(c, compute->x(), compute->y());
        unique->append(tmp);
        vertData->append(tmp);
        m_vbo.bind();
        m_vbo.write(0, unique->constData(), (unique->count() + 6)  * sizeof(GLfloat));
        update();
        qDebug() << "WHY DOES IT NOT SHOW ? : " << unique->size();
    }

    qDebug() << vertData->count();*/
    return vertData;
}

void myOpenGLWidget::makeGLObjects()
{
    QVector<float> *vertData = vecOfObj();
    m_vbo.create();
    m_vbo.setUsagePattern(QOpenGLBuffer::DynamicDraw);
    m_vbo.bind();

    //QVector<float> *vertData = initVector();
    int nbCurrentVertex = vertData->size()/6;
    if(nb_vertex != nbCurrentVertex){
        nb_vertex = nbCurrentVertex;
        m_vbo.create();
        m_vbo.setUsagePattern(QOpenGLBuffer::DynamicDraw);
        m_vbo.bind();
        m_vbo.allocate(vertData->constData(), (vertData->count() + 6) * sizeof(GLfloat));
    }else{
        m_vbo.bind();
        m_vbo.write(0, vertData->constData(), vertData->count()  * sizeof(GLfloat));
    }

}

void myOpenGLWidget::tearGLObjects()
{
	m_vbo.destroy();
}


void myOpenGLWidget::resizeGL(int w, int h)
{
	qDebug() << __FUNCTION__ << w << h;

	//C'est fait par défaut
	glViewport(0, 0, w, h);

	m_ratio = (double) w / h;
    m_proj.setToIdentity();
    m_proj.perspective(90.0f, m_ratio, 0.05f, 100.0f);
}

void myOpenGLWidget::paintGL()
{
	qDebug() << __FUNCTION__ ;

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	m_program->bind(); // active le shader program

    m_world.setToIdentity();
    m_world.setToIdentity();
    m_world.rotate(180.0f - (m_angle_X / 16.0f), 1, 0, 0);
    m_world.rotate(m_angle_Y / 16.0f, 0, 1, 0);
    m_world.rotate(m_angle_Z / 16.0f, 0, 0, 1);



    m_program->bind();
    m_program->setUniformValue(m_projMatrixLoc, m_proj);
    m_program->setUniformValue(m_mvMatrixLoc, m_camera * m_world);
    QMatrix3x3 normalMatrix = m_world.normalMatrix();
    m_program->setUniformValue(m_normalMatrixLoc, normalMatrix);

	m_program->setAttributeBuffer("posAttr", GL_FLOAT, 0, 3, 6 * sizeof(GLfloat));
	m_program->setAttributeBuffer("colAttr", GL_FLOAT, 3 * sizeof(GLfloat), 3, 6 * sizeof(GLfloat));
	m_program->enableAttributeArray("posAttr");
	m_program->enableAttributeArray("colAttr");

	glPointSize (5.0f);
    glDrawArrays(GL_QUADS, 0, nb_vertex);

	m_program->disableAttributeArray("posAttr");
	m_program->disableAttributeArray("colAttr");

	m_program->release();
}

void myOpenGLWidget::keyPressEvent(QKeyEvent *ev)
{
	qDebug() << __FUNCTION__ << ev->text();

	switch(ev->key()) {
		case Qt::Key_Z :
            m_angle += 0.1;
			if (m_angle >= 360) m_angle -= 360;
			update();
			break;
		case Qt::Key_A :
			if (m_timer->isActive())
				m_timer->stop();
			else m_timer->start();
			break;
        //Force le calcul d'un point sur notre surface de Bézier.
        case Qt::Key_C :
            makeGLObjects();
            update();
			break;
        case Qt::Key_M :
            X+=0.1f;
            makeGLObjects();
            update();
            break;
	}
}

void myOpenGLWidget::keyReleaseEvent(QKeyEvent *ev)
{
    //qDebug() << __FUNCTION__ << ev->text();
}

void myOpenGLWidget::mousePressEvent(QMouseEvent *ev)
{
    //qDebug() << __FUNCTION__ << ev->x() << ev->y() << ev->button();
}

void myOpenGLWidget::mouseReleaseEvent(QMouseEvent *ev)
{
    //qDebug() << __FUNCTION__ << ev->x() << ev->y() << ev->button();
}

void myOpenGLWidget::mouseMoveEvent(QMouseEvent *ev)
{
    //qDebug() << __FUNCTION__ << ev->x() << ev->y();
    //qDebug() << __FUNCTION__ << ev->x() << ev->y();
    int dx = ev->x() - m_last_pos.x();
       int dy = ev->y() - m_last_pos.y();

       if (ev->buttons() & Qt::LeftButton) {
           setXRotation(m_angle_X + 8 * dy);
           setYRotation(m_angle_Y + 8 * dx);
       } else if (ev->buttons() & Qt::RightButton) {
           setXRotation(m_angle_X - 8 * dy);
           setZRotation(m_angle_Z - 8 * dx);
       }
       m_last_pos = ev->pos();
}

void myOpenGLWidget::setPoint(float x, float y){
    protect = true;
    compute->setX(x);
    compute->setY(y);
    makeGLObjects();
    update();
}

void myOpenGLWidget::setStepU(int step){
    stepU = 1./step;
    makeGLObjects();
    update();
}

void myOpenGLWidget::setStepV(int step){
    stepV = 1./step;
    makeGLObjects();
    update();
}

void myOpenGLWidget::onTimeout()
{
    //qDebug() << __FUNCTION__ ;

	update();
}





