#include "carreau.h"
#include "bernstein.h"
#include "QDebug"
#include "bezier.h"

Carreau::Carreau(QVector<QVector<Point>> array)
    :array(array){}

Point Carreau::getPoint(float u, float v){
    int m = array.size();
    int n = array[0].size();
    qDebug() << "entree";
    Point a;
    //Quleque part ici on introduit des valeurs trop grandes ...
    for(int i=0; i<m; i++){

        float b2 = bernsteinCompute(i,m-1,v);
        qDebug() << "b2" <<b2;

        for (int j=0; j<n; ++j)
        {
        float b1 = bernsteinCompute(j,n-1,u);
        //Bezier b(array[i]);
        qDebug() << "b1" <<b1;
        float fac = b1 * b2;
        //Ici peut etre ?
        qDebug() << "LA CON DE MOI :" << fac;
        a = *(a + *(array[i][j] * fac));
        }
    }
    qDebug() << "sortie";
    return a;
}
