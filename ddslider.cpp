#include "ddslider.h"
#include <GL/glu.h>
#include <QDebug>
#include <QSurfaceFormat>

DDSlider::DDSlider(QWidget *parent) :
    QOpenGLWidget(parent)
{
    qDebug() << "init GLArea" ;

    // Ce n'est pas indispensable
    QSurfaceFormat sf;
    sf.setDepthBufferSize(24);
    setFormat(sf);
    qDebug() << "Depth is"<< format().depthBufferSize();
}

DDSlider::~DDSlider()
{
    qDebug() << "destroy GLArea";

    // Contrairement aux méthodes virtuelles initializeGL, resizeGL et repaintGL,
    // dans le destructeur le contexte GL n'est pas automatiquement rendu courant.
    makeCurrent();

    // ici destructions de ressources GL

    doneCurrent();
}


void DDSlider::initializeGL()
{
    qDebug() << __FUNCTION__ ;
    initializeOpenGLFunctions();
    glEnable(GL_DEPTH_TEST);
}

void DDSlider::resizeGL(int w, int h)
{
    qDebug() << __FUNCTION__ << w << h;

    // C'est fait par défaut
    glViewport(0, 0, w, h);

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    GLfloat hr = 0.5, wr = hr * w / h;
    glFrustum(-wr, wr, -hr, hr, 1.0, 5.0);
    glMatrixMode(GL_MODELVIEW);
}

void DDSlider::paintGL()
{
    qDebug() << __FUNCTION__ ;

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glLoadIdentity();
    gluLookAt (0.5, 0.5, 1, 0.5, 0.5, 0, 0, 1, 0);

    glBegin(GL_LINES);
    glColor3f (1.0, 0.6, 0.6);
    glVertex3f(-45,0.5,0);glVertex3f(45,0.5,0);
    glColor3f (0.1, 0.6, 0.3);
    glVertex3f(-45,0.25,0);glVertex3f(45,0.25,0);
    glColor3f (0.1, 0.6, 0.3);
    glVertex3f(-45,0.75,0);glVertex3f(45,0.75,0);

    glColor3f (1.0, 0.6, 0.6);
    glVertex3f(0.75, 45, 0); glVertex3f(0.75, -45, 0);
    glVertex3f(0.25, 45, 0); glVertex3f(0.25, -45, 0);
    glVertex3f(0.5, 45, 0); glVertex3f(0.5, -45, 0);
    glEnd();

    qDebug() << "PAINT GL" << current->x() << current->y();
    float delta = 0.0125f;

    glBegin(GL_TRIANGLE_FAN);
        glColor3f (0.0, 0.6, 1.0);
        glVertex3f(current->x(), current->y(), 0);
        glVertex3f(current->x() - delta, current->y() - delta, 0);
        glVertex3f(current->x() - delta, current->y() + delta, 0);
        glVertex3f(current->x(), current->y(), 0);
        glVertex3f(current->x() - delta, current->y() - delta, 0);
        glVertex3f(current->x() + delta, current->y() - delta, 0);
        glVertex3f(current->x(), current->y(), 0);
        glVertex3f(current->x() + delta, current->y() + delta, 0);
        glVertex3f(current->x() - delta, current->y() + delta, 0);
        glVertex3f(current->x(), current->y(), 0);
        glVertex3f(current->x() + delta, current->y() - delta, 0);
        glVertex3f(current->x() + delta, current->y() + delta, 0);
    glEnd();

}

void DDSlider::mousePressEvent(QMouseEvent *ev){
    //qDebug() << __FUNCTION__ << "DD" << ev->x() << ev->y() << ev->button();
    //On récupère un point de coordonnées comprises entre (0, 0) et (150, 150)
    //On ramène ces coordonnées à des valeurs comprises entre (0, 0) et (1, 1)
    //qDebug() << "MOUSE EVENT W/H" << this->width() << this->height();
    state = true;
    current->setX((float) ev->x() / (float) this->width());
    current->setY(1 - ((float) ev->y() / (float) this->height()));

    //qDebug() << "MOUSE EVENT" << current->x() << current->y();
    emit mouseClicked();
    mousePressed = true;
    update();
}

void DDSlider::mouseReleaseEvent(QMouseEvent *ev){
    state = false;
    current->setX((float) ev->x() / (float) this->width());
    current->setY(1 - ((float) ev->y() / (float) this->height()));

    //qDebug() << "MOUSE EVENT" << current->x() << current->y();
    emit mouseReleased();

    float nX = (float) ev->x() / (float) this->width();
    float nY = 1 - ((float) ev->y() / (float) this->height());

    current->setX(nX);
    current->setY(nY);

    emit posChanged(nX, nY);
    update();
    mousePressed = false;
}

void DDSlider::mouseMoveEvent(QMouseEvent *ev)
{
    if(mousePressed){
        float nX = (float) ev->x() / (float) this->width();
        float nY = 1 - ((float) ev->y() / (float) this->height());
        current->setX(nX);
        current->setY(nY);
        emit posChanged(nX, nY);
        update();
    }
}
