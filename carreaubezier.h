#ifndef CARREAUBEZIER_H
#define CARREAUBEZIER_H
#include "courbeparametrique.h"
#include "point.h"
#include "bezier.h"

class CarreauBezier
{
public:
    CarreauBezier(QVector<QVector<Point>> matrix);
    Point getPoint(float x, float y);
private:
    QVector<QVector<Point>> controlMatrix;
};

#endif // CARREAUBEZIER_H
