#include "bezier.h"

Bezier::Bezier(QVector<Point> points)
{
    this->points = points;
}

Point Bezier::getPoint(float x){

    QVector<Segment> segments = Segment::extractSegments(points);

    return  iterationCasteljaud(x, segments, 50);
}

Point Bezier::iterationCasteljaud(float x, QVector<Segment> segments, int level){
    if(level < 1 || segments.size()==1){return segments[0].getPoint(x);}
    QVector<Segment> newSegments = QVector<Segment>();

    Point a, b;
    a = segments[0].getPoint(x);
    for(int i=1; i<segments.size(); i++){
        b = segments[i].getPoint(x);
        newSegments.push_back(*Segment::createSegment(a,b));
        a = b;
    }

    return iterationCasteljaud(x, newSegments, level-1);
}
